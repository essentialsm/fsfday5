/**
 * Created by Leonard Cheong on 1/4/2016.
 */

var express = require("express");

var app = express ();

app.use(express.static(__dirname + "/public"));

app.listen(3000,function() {
    console.info("Application started on port 3000");
    console.info("Cntrl-C to terminate");
})