/**
 * Created by Leonard Cheong on 1/4/2016.
 */

// ARRAYS
    
var myarray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

// no. of elements in an array .length
/*
console.info("myarray = " + myarray.length);
console.info("daysOfWeek = " + daysOfWeek.length)
*/

// for loop
 /* for (var i = 0; i < daysOfWeek.length; i++) {
    console.info(daysOfWeek[i]);
}
*/

// while loop
/*
var p = 0;

while (p < daysOfWeek.length) {
    console.info(daysOfWeek[p]);
    p++
}
*/

var taskList = [];

taskList.push ("Learn JavaScript");
taskList.push ("Master JavaScript");
taskList.push ("Own JavaScript");

// push adds to the back of the array

taskList.unshift("Be a noob");

// unshift adds to the front of the array

// removing an element from the back taskList.pop()
// removing an element from the front taskList.shift()

/*
to model an array as a stack (last in last out), use push and pop
to model an array as a queue (first in first out), use push and shift
 */

console.log(taskList);

for (var i = 0; i < taskList.length; i++) {
    console.info(i + ". " + taskList[i]);
}
    if (i >= 4) {
        console.log("That's a lot to do");
        } else {
        console.log("Give me more tasks");
    }

var myLibrary = [myarray,daysOfWeek];

taskList.forEach(function(e){
    console.info(e)
})

// listing all elements in an array without using a for loop
